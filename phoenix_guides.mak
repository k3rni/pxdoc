docsets: guides-docset

docset-dirs: guides-docset-dir

guides-docset: guides-docset-content

guides-docset-dir: 
	mkdir -p ${DOCSET_ROOT}/phoenix_guides/style
	mkdir -p $(addprefix ${DOCSET_ROOT}/phoenix_guides/doc/, $(shell find deps/phoenix_guides -maxdepth 1 -mindepth 1 -type d | cut -d/ -f3))

guides-html-dir: 
	mkdir -p deps/phoenix_guides/doc

guides-html-dirs:
	mkdir -p $(addprefix deps/phoenix_guides/doc/, $(shell find deps/phoenix_guides -maxdepth 1 -mindepth 1 -type d | cut -d/ -f3))

deps/phoenix_guides/doc:
	mkdir $@

guides-docset-content: guides-docset-dir guides-html-files
	cp deps/phoenix_guides/doc/*.html ${DOCSET_ROOT}/phoenix_guides
	find deps/phoenix_guides/doc -maxdepth 1 -mindepth 1 -type d -exec cp -r {} ${DOCSET_ROOT}/phoenix_guides \;
	cp deps/phoenix_guides/styling/Phoenix_files/*.css ${DOCSET_ROOT}/phoenix_guides/style

deps/phoenix_guides/doc/%.html: deps/phoenix_guides/%.md
	python3 -m markdown $^ | cat boilerplate/html_header - boilerplate/html_footer > $@

guides-html-files: guides-html-dirs deps/phoenix_guides \
	$(subst .md,.html,$(subst phoenix_guides/,phoenix_guides/doc/,$(wildcard deps/phoenix_guides/*.md))) \
	$(subst .md,.html,$(subst phoenix_guides/,phoenix_guides/doc/,$(wildcard deps/phoenix_guides/**/*.md)))
