docsets: plug-docset

docset-dirs: plug-docset-dir

plug-docset: plug-docset-content

plug-docset-dir:
	mkdir -p ${DOCSET_ROOT}/plug

plug-docset-content: plug-docset-dir deps/plug/doc
	cp -r deps/plug/doc/* ${DOCSET_ROOT}/plug/
	rm -f $(addprefix ${DOCSET_ROOT}/plug/,404.html index.html)
	${STRIP_SIDEBAR} ${DOCSET_ROOT}/plug/*.html

deps/plug/doc: 
	cd deps/plug && yes | mix deps.get
	cd deps/plug && yes | MIX_ENV=docs mix docs
