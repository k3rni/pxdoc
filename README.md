# PhoenixFramework.docset

This is a script to build a Zeal/Dash docset for [Phoenix Framework](https://www.phoenixframework.org).

Included are:

* [Phoenix](https://github.com/phoenixframework/phoenix) itself 
* [Phoenix.HTML](https://github.com/phoenixframework/phoenix_html)
* [Phoenix.Ecto](https://github.com/phoenixframework/phoenix_ecto)
* [Ecto](https://github.com/elixir-lang/ecto)
* [Phoenix.PubSub](https://github.com/phoenixframework/phoenix_pubsub)
* [Plug](https://github.com/elixir-lang/plug)
* [Phoenix Guides](https://github.com/phoenixframework/phoenix_guides)

# Installing

1. On GitLab, go into [Builds](https://gitlab.com/k3rni/pxdoc/builds).
2. Click the download icon on a line with a passed build. This is a build artifacts file, which contains `Phoenix.tgz`
3. Unpack `Phoenix.tgz` into your docset directory, typically `~/.local/share/Zeal/Zeal/docsets`
4. Restart Zeal, enjoy your new documentation

## Easy mode

[Copy this link](http://pxdoc.herokuapp.com/feed). Open Zeal's docset configuration, click _Add Feed_ and paste the link. Confirm and wait a bit - the docset is now installed.

# Building

## Prerequisites

* Python 3
* Recent Elixir and Mix, I recommend using [Erlang Solutions' repositories](https://www.erlang-solutions.com/resources/download.html)
* [Zeal](https://zealdocs.org)

## Steps

1. Install the required Python packages (`pip install -r requirements.txt`)
2. If running for the first time, run `git submodule init` and then `git submodule update` in the project directory. Otherwise just the latter.
2. Run `make` in the project directory. This may take a while
3. Copy or symlink the generated `PhoenixFramework.docset` directory into your Zeal docset dir. This is `~/.local/share/Zeal/Zeal/docsets` on modern Linux versions
4. Restart Zeal, enjoy your new documentation
