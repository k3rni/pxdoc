#! /usr/bin/env python3
# encoding: utf-8

import sys, re, codecs
from pathlib import Path
from bs4 import BeautifulSoup

# map to dash concepts
map_kinds = dict(
    # Plurals are for the list sections
    macros='Macro',
    functions='Function', # method?
    types='Type',
    callbacks='Callback',
    # Singulars are for an entire file unit name
    module='Module',
    behaviour='Interface',
    exception='Exception',
    protocol='Protocol',
    file='File',
    guide='Guide'
)
interesting_sections = map_kinds.keys()

def display_insert_query(symbol, kind, path):
    query = """INSERT OR IGNORE INTO searchIndex (name, type, path) VALUES ('%s', '%s', '%s');""" % (symbol, kind, path)
    print(query)

def transform_sig(sig):
    return sig.replace('c:', '')

def transform_filename(name):
    letter, _, rest = name.partition('_')
    if rest:
        title = rest.replace('_', ' ').title()
        return '%s: %s' % (letter, title)
    else:
        return name.title()

def process_section(path, section, modulename):
    kind = section['id']
    for item in section.find_all('div', class_='detail'):
        sig = item['id']

        symbol = "%s.%s" % (modulename, transform_sig(sig))
        display_insert_query(symbol, map_kinds[kind], "%s#%s" % (path, sig))

def process_module(path, name, kind):
    display_insert_query(name, map_kinds[kind], path)

def process_file(filepath):
    print("-- %s" % filepath)
    with codecs.open(str(filepath), 'r', 'utf-8') as f:
        soup = BeautifulSoup(f, 'html5lib')
        content = soup.find('div', id='content')
        if not content: 
            # Probably a guide file
            process_module(filepath, transform_filename(Path(filepath).stem), 'guide')
            return

        header = content.find('h1')
        if not header: return
        head = list(header.stripped_strings)

        if len(head) == 3:
            # A module tagged with kind
            _ver, modulename, kind = head
        elif len(head) == 2:
            # Untagged module, a regular one
            _ver, modulename = head
            kind = 'module'
        else:
            modulename, kind = '%s/%s' % (head[0], Path(filepath).name), 'file'
        process_module(filepath, modulename, kind)

        for section in soup.find_all('section'):
            if section.get('id', None) not in interesting_sections: continue
            process_section(filepath, section, modulename)

def process_dirs(paths):
    print('-- //%s' % ':'.join(paths))
    for path in [Path(p) for p in paths]:
        print("-- /%s" % path)
        htmls = path.glob('**/*.html')
        for html in htmls:
            process_file(html)


if __name__ == "__main__":
    if len(sys.argv) == 0:
        paths = ["."]
    else:
        paths = sys.argv[1:]
    process_dirs(paths)

