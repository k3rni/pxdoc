docsets: ecto-docset

docset-dirs: ecto-docset-dir

ecto-docset: ecto-docset-content

ecto-docset-dir: 
	mkdir -p ${DOCSET_ROOT}/ecto

ecto-docset-content: ecto-docset-dir deps/ecto/doc
	cp -r deps/ecto/doc/* ${DOCSET_ROOT}/ecto/
	rm -f $(addprefix ${DOCSET_ROOT}/ecto/,404.html index.html)
	${STRIP_SIDEBAR} ${DOCSET_ROOT}/ecto/*.html

deps/ecto/doc: deps/ecto
	ls -lR
	cd deps/ecto && yes | mix deps.get
	cd deps/ecto && yes | MIX_ENV=docs mix docs
