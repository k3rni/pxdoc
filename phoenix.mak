docsets: phoenix-docset

docset-dirs: phoenix-docset-dir

phoenix-docset: phoenix-docset-content

phoenix-docset-dir:
	mkdir -p $(addprefix ${DOCSET_ROOT}/,phoenix phoenix_html phoenix_ecto phoenix_pubsub)

phoenix-docset-content: phoenix-docset-dir deps/phoenix/doc deps/phoenix_html/doc deps/phoenix_ecto/doc deps/phoenix_pubsub/doc
	cp -r deps/phoenix/doc/* ${DOCSET_ROOT}/phoenix/
	rm -f $(addprefix ${DOCSET_ROOT}/phoenix/,404.html index.html)
	${STRIP_SIDEBAR} ${DOCSET_ROOT}/phoenix/*.html
	cp -r deps/phoenix_html/doc/* ${DOCSET_ROOT}/phoenix_html/
	rm -f $(addprefix ${DOCSET_ROOT}/phoenix_html/,404.html index.html)
	${STRIP_SIDEBAR} ${DOCSET_ROOT}/phoenix_html/*.html
	cp -r deps/phoenix_ecto/doc/* ${DOCSET_ROOT}/phoenix_ecto/
	rm -f $(addprefix ${DOCSET_ROOT}/phoenix_ecto/,404.html index.html)
	${STRIP_SIDEBAR} ${DOCSET_ROOT}/phoenix_ecto/*.html
	cp -r deps/phoenix_pubsub/doc/* ${DOCSET_ROOT}/phoenix_pubsub/
	rm -f $(addprefix ${DOCSET_ROOT}/phoenix_pubsub/,404.html index.html)
	${STRIP_SIDEBAR} ${DOCSET_ROOT}/phoenix_pubsub/*.html

deps/phoenix/doc: deps/phoenix
	cd deps/phoenix && yes | mix deps.get
	cd deps/phoenix && yes | MIX_ENV=docs mix docs

deps/phoenix_html/doc: deps/phoenix_html
	cd deps/phoenix_html && yes | mix deps.get
	cd deps/phoenix_html && yes | MIX_ENV=docs mix docs

deps/phoenix_ecto/doc: deps/phoenix_ecto
	# Ecto doesn't ship with ExDoc by default. Sneak it into mix.exs
	sed -i -f patches/add_exdoc.sed deps/phoenix_ecto/mix.exs
	cd deps/phoenix_ecto && yes | mix deps.get
	cd deps/phoenix_ecto && yes | MIX_ENV=docs mix docs

deps/phoenix_pubsub/doc: deps/phoenix_pubsub
	sed -i -f patches/add_exdoc.sed deps/phoenix_pubsub/mix.exs
	cd deps/phoenix_pubsub && yes | mix deps.get
	cd deps/phoenix_pubsub && yes | mix docs
