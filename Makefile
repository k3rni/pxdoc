ROOT=PhoenixFramework.docset
DOCSET_ROOT=${ROOT}/Contents/Resources/Documents
ICON_LOCATION=${ROOT}/icon.png
PLIST_LOCATION=${ROOT}/Contents/Info.plist
JSON_LOCATION=${ROOT}/meta.json
INDEX_LOCATION=${ROOT}/Contents/Resources/docSet.dsidx
STRIP_SIDEBAR=./strip_sidebar.py
INDEXER=./dashindexer.py

.PHONY: clean clean-index

all: plist icon json clean-index fill-index package

include *.mak

clean:
	rm -rf ${DOCSET_ROOT} ${INDEX_LOCATION} ${PLIST_LOCATION} ${ICON_LOCATION} ${JSON_LOCATION}

clean-index:
	rm -rf ${INDEX_LOCATION}

build-dir:
	mkdir -p build/

root: ${DOCSET_ROOT}

${DOCSET_ROOT}:
	mkdir -p ${DOCSET_ROOT}

plist: root ${PLIST_LOCATION}

${PLIST_LOCATION}:
	cp -f pxdoc.plist $@

icon: root ${ICON_LOCATION}

${ICON_LOCATION}:
	cp -f icon.png $@

json: root ${JSON_LOCATION}

${JSON_LOCATION}:
	cp -f docset.json $@

package: Phoenix.tgz

debug-sql: 
	sqlite3 ${INDEX_LOCATION}

Phoenix.tgz: index-file
	tar czf $@ ${ROOT}

index-file: ${INDEX_LOCATION}

${INDEX_LOCATION}:
	sqlite3 ${INDEX_LOCATION} "CREATE TABLE searchIndex(id INTEGER PRIMARY KEY, name TEXT, type TEXT, path TEXT);"
	sqlite3 ${INDEX_LOCATION} "CREATE UNIQUE INDEX anchor ON searchIndex (name, type, path);"

debug-index: docsets docset-dirs 
	${INDEXER} ${DOCSET_ROOT} 

fill-index: docsets docset-dirs index-file
	${INDEXER} ${DOCSET_ROOT} | sed -e 's%${DOCSET_ROOT}/%%' | sqlite3 ${INDEX_LOCATION}
