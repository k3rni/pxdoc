#! /usr/bin/env python3
# encoding: utf-8

# Remove the sidebar section in specifed HTML files, in-place

import sys, codecs
from bs4 import BeautifulSoup

count = 0
for filename in sys.argv[1:]:
    sys.stderr.write("* %s\n" % filename)
    html = ''
    with codecs.open(filename, 'r', 'utf-8') as fp:
        soup = BeautifulSoup(fp, 'html5lib')
        sidebar = soup.find('section', class_='sidebar')
        if sidebar:
            sidebar.extract()

        sidebar_toggle = soup.find('button', class_='sidebar-toggle')
        if sidebar_toggle:
            sidebar_toggle.extract()
        html = soup.prettify()

    with codecs.open(filename, 'wb', 'utf-8') as fp:
        fp.write(html)

    count += 1

print('Processed %d files' % count)
